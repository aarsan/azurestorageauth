# SET ACCOUNT NAME, ACCESS KEY, AND VERSION
$accountname = "fileservicetest"
$accesskey = 'LPDrZQh0ndCJ6VU6QluJyy7v0KeMI+1Xeso3OkK39RmXR1I67VL4Rxaq1r+zeUK4bH5uRyba+/QCV9D22nGotw=='
$version = "2014-02-14"

$listshares = ""

#GET UTC TIME IN PROPER FORMAT
$xmsdate = get-date
$xmsdate = $xmsdate.ToUniversalTime()
$xmsdate = $xmsdate.toString('r')

# DEFINE NEW LINE CHAR AND PROPERLY FORMAT THE MESSAGE TO BE ENCRYPTED
$nl = $([char]10);
$message = "GET" + $nl + $nl + $nl + $nl + $nl + $nl + $nl + $nl + $nl + $nl + $nl + $nl + "x-ms-date:" + $xmsdate + $nl + "x-ms-version:2014-02-14" + $nl + "/" + $accountname + "/" + $nl + "comp:list"

# ENCRYPT THE MESSAGE AND BUILD THE SIGNATURE
$hmacsha = New-Object System.Security.Cryptography.HMACSHA256
$hmacsha.key = [Convert]::FromBase64String($accesskey)
$signature = $hmacsha.ComputeHash([Text.Encoding]::UTF8.GetBytes($message))
$signature = [Convert]::ToBase64String($signature)

# BUILD THE HEADER AND INCLUDE THE SIGNATURE
$headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
$headers.Add("x-ms-version", $version)
$headers.Add("x-ms-date", $xmsdate)
$headers.Add("Authorization", "SharedKey " + $accountname + ":" + $signature)

# BUILD THE URI
$uri = "http://" + $accountname + ".file.core.windows.net/?comp=list"

# MAKE THE REST CALL
invoke-restmethod -Uri $uri -Headers $headers